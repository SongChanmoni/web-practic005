import React from 'react'
import { Card, Button, Badge } from 'react-bootstrap';

function Item(props) {
    return (
        <Card>
            <Card.Img variant="top" src={props.item.img} />
            <Card.Body>
                <Card.Title><h3>{props.item.title}</h3></Card.Title>
                <Card.Text>
                    Price: {props.item.price} $
                </Card.Text>
               
                <Button
                    variant="primary"
                    className="mx-2"
                    onClick={() =>
                        props.onAdd(props.idx)
                    }>Add</Button>
                <Button
                    variant="warning"
                    className="mx-2"
                    disabled={props.item.amount === 0}
                    onClick={() => {
                        props.onDelete(props.idx)
                    }}
                >Delete</Button>
                <Button
                    variant="danger"
                    disabled={props.item.amount === 0}
                    onClick={() => {
                        props.onDelete(props.idx)
                    }}
                >{props.item.amount}</Button>
                <Button className="mt-3 mx-5" variant="success">Total: {props.item.total} $</Button>
              
            </Card.Body>
        </Card>
    )
}

export default Item
