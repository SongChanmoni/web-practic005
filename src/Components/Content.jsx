import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import Item from './Item'
import MyTable from './MyTable'

function Content(props) {

    return (
        <Container>
            <h1 className="my-5 text-center text-warning">Popular Product</h1>
            <Row>
                {
                    props.items.map((item, idx) =>
                        <Col md={3} key={idx}>
                            <Item idx={idx} item={item} onAdd={props.onAdd} onDelete={props.onDelete} />

                        </Col>
                    )
                }
            </Row>
            <Row>
                {
                    <Col md={12} >
                    <MyTable />
                    </Col>
                }
            </Row>


        </Container>
    )
}

export default Content
