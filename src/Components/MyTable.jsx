import React from 'react'
import { Table } from 'react-bootstrap'

export default function MyTable() {

    return (
        <div>
            <Table striped bordered hover variant="dark text-center">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Bule Dress</td>
                        <td>2</td>
                        <td>4000 $</td>
                        <td>8000 $</td>
                    </tr>

                </tbody>
            </Table>

        </div>
    )
}
