import './App.css';
import React, { Component } from 'react'
import Content from './Components/Content'
import 'bootstrap/dist/css/bootstrap.min.css';
import Menu from './Components/Menu';
import MyTable from './Components/MyTable';

export default class App extends Component {
  constructor() {
    super()
    this.state = {
      data: [
        {
          id: 1,
          title: "Bule Dress",
          amount: 0,
          img: 'image/dress1.jpg',
          price: 4000,
          total: 0,
        },
        {
          id: 2,
          title: "Nice Dress",
          amount: 0,
          img: "image/dress2.webp",
          price: 300,
          total: 0,
        },
        {
          id: 3,
          title: "Bule Suit",
          amount: 0,
          img: "image/suit6.jpg",
          price: 100,
          total: 0,
        },
        {
          id: 4,
          title: "Nice Suit",
          amount: 0,
          img: "image/suit5.webp",
          price: 1200,
          total: 0,
        }
      ]
    }
    this.onAdd = this.onAdd.bind(this)
    this.onDelete = this.onDelete.bind(this)
  }

  onAdd(index) {
    let data = this.state.data
    data[index].amount++
    data[index].total = data[index].amount * data[index].price
    this.setState({
      data: data
    })

  }
  onDelete(index) {
    let data = this.state.data
    data[index].amount--
    data[index].total = data[index].amount * data[index].price
    this.setState({
      data: data
    })
  }

  render() {
    return (
      <div>
        <Menu />
        <Content 
          onAdd={this.onAdd}
          onDelete={this.onDelete}
          items={this.state.data} />
        
      </div>
    )
  }
}

